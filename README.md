The Rigveda: complete metrically restored Sanskrit text in roman transliteration
================================================================================

![book cover](rgveda-metric-cover.jpg)

### ebook notes

This ebook is built from the online Sanskrit text of the Rigveda available
at: <http://www.utexas.edu/cola/centers/lrc/RV/>, edited by Karen
Thomson and Jonathan Slocum, based on *Rig Veda: a Metrically Restored
Text*, by Barend A. van Nooten and Gary B. Holland (Harvard Uni. Press,
1994). As Thomson and Slocum allude in their introduction (below), the
van Nooten/Holland edition would have been based on the electronic text
"found" at the Oxford Text Archive (OTA), which itself originated at the
University of Texas and is presumably the edition compiled by H.S.
Ananthanarayana and W.P. Lehman, based on Theodor Aufrecht's 1877
edition ([1st vol](https://archive.org/details/diehymnendesrig00aufrgoog),
[2nd vol](https://archive.org/details/diehymnendesrig01aufrgoog)). (Note,
of course, that the edition produced herein is \*not\* Aufrecht 1877
anymore, as it has been extensively revised, by van Nooten and Holland,
and then by Thomson and Slocum.)

My intention behind creating this document was to have an epub version
of the original Sanskrit text of the Rigveda (easily portable, viewable
on many devices). Ideally this would be accompanied by a facing
translation. However, excepting for the most recently published
translation (Jamison and Brereton 2014), existing English translations
(not under copyright) are woefully inadequate.

The translation recommended by the author of these introductory notes (B. Slade) is that of
[Stephanie W. Jamison and Joel P.  Brereton (2014): *The Rigveda: the earliest religious poetry of India* (Oxford Uni. Press)](https://global.oup.com/academic/product/the-rigveda-9780199370184).
See also their supplemental and freely available commentary at:
<http://rigvedacommentary.alc.ucla.edu/>.

The second best option for a translation is Karl Friedrich Geldner's *Der
Rig-Veda aus dem Sanskrit ins Deutsche übersetzt* (originally completed in 1928,
but not published until 1951, Harvard Uni. Press; reprinted 2003), which is
available freely on the web at <http://www.thombar.de/> (the site suggests that
the creator, Thomas Barth, modified Geldner's text in some fashion, but exactly
to what extent is not clear; I assume fairly minimally - one obvious unfortunate
difference is the lack of notes in the online edition). In theory this text
perhaps could be used in a future facing-translation edition, though for me an
English language version would be preferable.

Please let me know if you experience difficulties using this ebook.

The cover images are:

(top) Indra and Indrani, 15th century (Malla era), Kathmandu (Nepal)

from:
<https://commons.wikimedia.org/wiki/File:Indra_et_Indrani_(Mus%C3%A9e_des_arts_asiatiques,_Nice)_(5939561892).jpg>

(bottom) Yajna being performed at Vishnu Yangna Kunda on the occasion of
Kumbhabhishekam of renovated Gunjanarsimhaswamy Temple at Tirumakudal
Narsipur, Mysore (Karnataka, India), ca. 2011.

from:
<https://en.wikipedia.org/wiki/Agni#/media/File:Vishnu_Yagna_Kunda.jpg>

The cover font is Cardo ( <http://scholarsfonts.net/cardofnt.html> )

The font used throughout the book is Junicode (
<http://junicode.sourceforge.net/> ), excepting the mandala headings,
which are set in Linux Biolinum
(<http://www.linuxlibertine.org/index.php?id=86%27A%3D0>).

### Benjamin Slade

Version 0.1

Send comments etc. to: "beslayed at jnanam dot net" (replace "at" and
"dot" with the appropriate symbols)

See project's git page here: [https://gitlab.com/emacsomancer/rig-veda-metrically-restored-ebook](https://gitlab.com/emacsomancer/rig-veda-metrically-restored-ebook).

21-July-2015

[![Creative Commons
License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-sa/4.0/)  
<span dct="http://purl.org/dc/terms/"
href="http://purl.org/dc/dcmitype/Text" property="dct:title"
rel="dct:type">The Rigveda: complete metrically restored Sanskrit text
in roman transliteration</span> by <span
cc="http://creativecommons.org/ns#"
property="cc:attributionName">Benjamin Slade</span> is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International
License](http://creativecommons.org/licenses/by-nc-sa/4.0/).
